//
//  ViewController.m
//  delegatewithoutsegue
//
//  Created by daffomac on 2/25/16.
//  Copyright © 2016 demos. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//- (IBAction)firstButton:(id)sender {
//    SecondViewController *svc=[[SecondViewController alloc]init];
//    //svc.delegate=self;
//    [self.navigationController pushViewController:svc animated:YES];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)ShowSecondButton:(id)sender {
//  
//    UINavigationController *nav = [[UINavigationController alloc] init];
//   SecondViewController *svc=[[SecondViewController alloc]initWithNibName:@"SecondViewController" bundle:nil ];
//    [nav pushViewController:svc animated:NO];
//    [self presentViewController:nav animated:YES completion:nil];
    //SecondViewController *svc=[[SecondViewController alloc]initWithNibName:@"SecondViewController" bundle:nil ];
   // ViewController *vc=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil ];
    //svc.from = from;
    SecondViewController *svc =
    [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];

    //UINavigationController *navController = self.navigationController;
   // [navController pushViewController:vc animated:NO];
    [self.navigationController pushViewController:svc animated:NO];
    svc.delegate=self;
    //[self presentViewController:svc animated:YES completion:nil];
    
}
-(void)inputwith:(SecondViewController *)controller input:(NSString *)input
{
    self.showLabel.text=input;
    NSLog(@"inside input with declared in ViewController");
}
@end
