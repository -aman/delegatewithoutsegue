//
//  SecondViewController.h
//  delegatewithoutsegue
//
//  Created by daffomac on 2/25/16.
//  Copyright © 2016 demos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SecondViewController;
@protocol secondvcDelegate <NSObject>
@required
-(void)inputwith:(SecondViewController *)controller input:(NSString *)input;
@end
@interface SecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *inputBox;

@property (nonatomic,weak) id <secondvcDelegate> delegate;
@end
